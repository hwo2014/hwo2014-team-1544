import json
import socket
import sys
import time
import math

from BotBase import BotBase
from Car import Car
from SpeedOmeter import SpeedOmeter
from TrackObjects import Position, TrackData, Piece
from MathsHelpers import calculateDriveAndFrictionCoefficents, calculateCruiseThrottle, calculateDriftCoefficients, calculateMaxSpeedForDrift
from Turbo import Turbo
from AI import AI

class RaceBot(BotBase):

    STATE_CALIBRATE_DRIVE_FRICTION = 0
    # STATE_RACE will also work for qualifying.  It choses the optimal strategy
    # based on track state.  If there are no other cars then that's basically
    # floor it!
    STATE_COLLECT_DRIFT_SAMPLES = 2

    STATE_RACE = 3

    CALIBRATION_THROTTLE = 1.0
    COLLECT_DRIFT_SAMPLES_SPEED = 7
    CRUISE_SPEED = 6.3


    def __init__(self, socket, name, key):
        BotBase.__init__(self, socket, name, key)
        self.speed = 0
        self.track_data = None
        self.our_car_color = ""
        self.cars = {}
        self.tasks = []
        self.my_car = None
        self.crashed = False
        self.stored_turbo = None
        self.active_turbo = None
        self.ai = AI(self)

        # This list holds the values in the dictionary self.cars but sorted in
        # position order, 1st place being at position 0.
        self.car_positions = []
        self.car_positions_dirty = True

        self.state = self.STATE_CALIBRATE_DRIVE_FRICTION

        # TODO - maybe put these in SpeedOmeter class
        self.frictionCoefficient = 0
        self.driveCoefficient = 0
        self.baseFrictionCoefficient = 0
        self.baseDriveCoefficient = 0
        self.switchPending = False


        # TODO move these to a better place
        # For calculating drift logic
        self.driftSamples = []
        self.driftAccelSamples = []
        self.radiusSamples = []
        self.driftSamplesStarted = True
        self.driftSamplesCollected = False
        self.driftCo1 = 0
        self.driftCo2 = 0
        self.driftCo3 = 0
        self.driftShift = 0

        self.bendToSample = []

    # TODO: This should be in another class, but I'm too tired to figure out OOD!
    def get_car_positions(self):
        if self.car_positions_dirty == False:
            return self.car_positions

        # N.B. This works by sorting the cars in order, but with the first car
        # first in the list.  So the predicate to compare cars says "less than"
        # if a car is in front
        track_data = self.track_data
        def car_position_comparison(lhs, rhs):
            if lhs.position.lap > rhs.position.lap:
                return -1
            elif lhs.position.lap < rhs.position.lap:
                return 1

            if lhs.position.piece_index > rhs.position.piece_index:
                return -1
            elif lhs.position.piece_index < rhs.position.piece_index:
                return 1

            rhs_percent = rhs.percentage_into_track_piece(track_data)
            lhs_percent = lhs.percentage_into_track_piece(track_data)
            if lhs_percent > rhs_percent:
                return -1
            elif rhs_percent < rhs_percent:
                return 1
            else:
                return 0

        self.car_positions = sorted(self.cars.values(), car_position_comparison)
        self.car_positions_dirty = False
        return self.car_positions

    # TODO: This should be in another class, but I'm too tired to figure out OOD!
    def race_position(self):
        return self.get_car_positions().index(self.my_car) + 1

    # TODO: This should be in another class, but I'm too tired to figure out OOD!
    def lanes_of_cars_ahead(self):
        lanes = []
        car_positions = self.get_car_positions()
        our_car_index = self.race_position() - 1
        for car_ahead in reversed(car_positions[0: our_car_index]):
            lanes.append(car_ahead.position.end_lane_index)
        return lanes

    # TODO: Not sure if this should be a member of the bot or the car
    def apply_turbo_factor(self, turbo_factor):
        self.frictionCoefficient = turbo_factor * self.baseFrictionCoefficient
        self.driveCoefficient = turbo_factor * self.baseDriveCoefficient

    def activate_turbo(self):
        # These aren't errors according to the server, but they're errors in our
        # logic so I'm not allowing them
        if self.active_turbo:
            print("Error: tried to turbo whilst already turboing")
            return False
        if not self.stored_turbo:
            print("Error: tried to turbo without a turbo")
            return False

        print("Activating Turbo")
        self.active_turbo = self.stored_turbo
        self.stored_turbo = None
        self.apply_turbo_factor(self.active_turbo.turbo_factor)
        self.active_turbo.start(self.gameTick)
        self.turbo()

    def on_turbo_available(self, data):
        if self.crashed == True:
            print("Wasted turbo: Car off track")
            return

        if self.stored_turbo:
            print("Wasted turbo: Already have an unused turbo")
            return

        self.stored_turbo = Turbo(data, self.gameTick)
        print("Turbo available")
        print(self.stored_turbo)

    def tick_turbo(self):
        if not self.active_turbo:
            return

        if not self.active_turbo.exhausted(self.gameTick):
            return

        self.apply_turbo_factor(1.0)
        self.active_turbo = None

    def on_spawn(self, data):
        self.crashed = False
        # TODO: Reset car position somehow!

    def on_crash(self, data):
        self.crashed = True
        self.printStats()
        # TODO: Reset the car speed.  Position should be reset by spawn BUT
        # we might need to sort out the prev and curr positions.

    def on_car_positions(self, data):
        #start = time.clock()

        self.tick_turbo()

        for car_data in data:
            car = self.cars[car_data["id"]["color"]]
            car.set_position(Position(car_data["piecePosition"]), car_data["angle"], self.gameTick)
            self.car_positions_dirty = True
            #if self.gameTick % 60 == 0:
            #    print("%s car speed is %s" % (car_data["id"]["color"], car.speedo.speed()))
            # TODO Use lap data?

        # I would expect us to get carPositions even when off track, but maybe not!
        if self.crashed:
            self.ping()
            return

        # Don't need to calculate this every tick.
        self.my_car = self.cars[self.our_car_color]

        speed = self.my_car.speedo.speed()
        accel = self.my_car.speedo.accel()

        if self.state == self.STATE_CALIBRATE_DRIVE_FRICTION:
            prevSpeed = car.speedo.prevSpeed()
            prevAccel = car.speedo.prevAccel()
            if prevSpeed > 0 and prevAccel > 0 and speed > 0 and accel > 0 and self.gameTick > 5:
                self.baseDriveCoefficient, self.baseFrictionCoefficient = calculateDriveAndFrictionCoefficents(self.CALIBRATION_THROTTLE, prevSpeed, prevAccel, speed, accel)
                print "Drive, Friction Coefficients:", self.baseDriveCoefficient, self.baseFrictionCoefficient
                self.apply_turbo_factor(1.0)
                self.printStats()
                self.state = self.STATE_COLLECT_DRIFT_SAMPLES
                self.bendToSample = self.track_data.getNextCompositeBend(self.my_car.position)

        elif self.state == self.STATE_COLLECT_DRIFT_SAMPLES:
            #if len(self.bendToSample) <= 0:
            #    self.state = self.STATE_RACE

            position = self.my_car.position

            currentPiece = self.track_data.pieces[position.piece_index]
            if position.piece_index > self.bendToSample[-1]:
                self.driftSamplesCollected = True
                # finished collecting samples, so calulcate drift coefficients
                self.driftCo1, self.driftCo2, self.driftCo3, self.driftShift = calculateDriftCoefficients(speed, self.driftSamples, self.driftAccelSamples, self.radiusSamples)
                print "MAYUR:", self.driftCo1, self.driftCo2, self.driftCo3, self.driftShift
                self.state = self.STATE_RACE
            elif currentPiece.piece_type == Piece.TYPE_CORNER:
                self.collectedDriftSample()
        
        #elif self.state == self.STATE_RACE:
            #if self.gameTick % 60 == 0:
                #self.printStats()

        #print "bestlanes: ", self.decideLaneSwitchDirectionForOptimalLine()

        # Very hacky for now.  Preserving as much of the old code as possible to
        # make integration easier for the others.
        if self.state in (self.STATE_CALIBRATE_DRIVE_FRICTION, self.STATE_COLLECT_DRIFT_SAMPLES):
            self.decideMove()
        else:
            self.ai.tick()

        #end = time.clock()
        #elapsed = end - start

        #print "timeForMove", elapsed

    def on_lap_finished(self, data):
        print data
        self.ping()

    def on_your_car(self, data):
        self.our_car_color = data["color"]
        print("Our car has color [%s]" % self.our_car_color)

    def on_game_init(self, data):
        self.track_data = TrackData(data["race"]["track"])
        for car in data["race"]["cars"]:
            new_car = Car(car)
            new_car.create_speedo(SpeedOmeter, self.track_data)
            self.cars[car["id"]["color"]] = new_car

    def run(self):
        print("Custom run function for RaceBot")
        BotBase.run(self)


    def decideMove(self):
        if self.my_car.speedo.laneChanged():
            self.switchPending = False

        if self.state not in (self.STATE_CALIBRATE_DRIVE_FRICTION, self.STATE_COLLECT_DRIFT_SAMPLES) and not self.switchPending:
            switchDirection = self.decideLaneSwitchDirectionForOptimalLine()
            if switchDirection != 0:
                self.switch_lane(toRight=switchDirection == 1)
                self.switchPending = True
                self.printStats()
                return

        self.throttle(self.decideThrottle())

    def decideThrottle(self):
        speed = self.my_car.speedo.speed()

        if self.state == self.STATE_CALIBRATE_DRIVE_FRICTION:
            throttle = self.CALIBRATION_THROTTLE
        elif self.state == self.STATE_COLLECT_DRIFT_SAMPLES:
            throttle = calculateCruiseThrottle(speed, self.COLLECT_DRIFT_SAMPLES_SPEED,
                        self.frictionCoefficient, self.driveCoefficient, allowOverShoot=False)
        elif self.state == self.STATE_RACE:
            # TODO: TEMP: HACK HACK HACK.  Just want to get the car flying
            pieces = self.track_data.pieces
            i = self.my_car.position.piece_index
            ii = (i + 1) % len(pieces)
            # iii = (i + 2) % len(pieces)
            if False: #pieces[i].piece_type == Piece.TYPE_STRAIGHT and pieces[ii].piece_type == Piece.TYPE_STRAIGHT:
                # and pieces[iii].piece_type == Piece.TYPE_STRAIGHT:
                # Max it out!
                throttle = 1.0
            #elif speed > self.CRUISE_SPEED:
            #    throttle = 0.0
            #else:
            ##    throttle = calculateCruiseThrottle(speed, self.CRUISE_SPEED,
            #        self.frictionCoefficient, self.driveCoefficient, allowOverShoot=False)
            else: # Stick to safe maxVelocity for upcoming bend
                speedo = self.my_car.speedo
                accel = speedo.accel()
                driftAngle = self.my_car.angle
                driftAngularSpeed = speedo.angularSpeed()
                driftAngularAccel = speedo.angularAccel()
                maxSafeSpeed = calculateMaxSpeedForDrift(self.my_car.position, self.track_data, speed, accel, driftAngle, driftAngularSpeed, driftAngularAccel, 
                                            self.frictionCoefficient, self.driftCo2, self.driftCo3, self.driftShift)

                throttle = calculateCruiseThrottle(speed, maxSafeSpeed,
                        self.frictionCoefficient, self.driveCoefficient, allowOverShoot=False)

                #print "maxSafeSpeed, throttle:", maxSafeSpeed, throttle

        # Return capped throttle
        return  max(min(throttle, 1.0), 0)

    def decideLaneSwitchDirectionForOptimalLine(self):
        pieceIndex = self.my_car.position.piece_index
        piece = self.track_data.pieces[pieceIndex]
        #piece = getCurrentPiece()
        if True: #piece.hasSwitch:
            currentLane = self.my_car.position.end_lane_index
            bestLaneLengths = self.getBestLaneLengths()
            currLaneLength = bestLaneLengths.get(currentLane, 10000000000)

            # starting from the beginning (shortest lane), see which one we can switch to
            for laneIndex in bestLaneLengths.keys():
                # If already on best lane or there is no difference in length then no need to switch
                if laneIndex == currentLane or abs(bestLaneLengths[laneIndex] - currLaneLength) < 0.01:
                    break

                laneOffset = laneIndex - currentLane
                if abs(laneOffset) == 1:
                    return laneOffset

        return 0

    def getBestLaneLengths(self):
        nextSwitchPieceIndices = self.track_data.getNextSwitches(self.my_car.position, 2)
        # TODO: This doesn't return anything if len < 2.
        if len(nextSwitchPieceIndices) >= 2:
            return self.track_data.getLanesLengthsBetweenPieces(nextSwitchPieceIndices[0], nextSwitchPieceIndices[1])

    def printStats(self):
        speed = self.my_car.speedo.speed()
        accel = self.my_car.speedo.accel()
        driftAngle = self.my_car.angle
        angularSpeed = self.my_car.speedo.angularSpeed()
        angularAccel = self.my_car.speedo.angularAccel()
        currentLane = self.my_car.position.end_lane_index
        pieceIndex = self.my_car.position.piece_index
        lengthOfNextBend = self.track_data.getLengthOfNextCompositeBend(self.my_car.position)
        print pieceIndex, lengthOfNextBend, currentLane, speed, accel, driftAngle, angularSpeed, angularAccel


    # TODO - move this to a a better place
    # Drift calculation logic:
    def collectedDriftSample(self):
        pieces = self.track_data.pieces
        positon = self.my_car.position
        pieceIndex = positon.piece_index

        speed = self.my_car.speedo.speed() 
        driftAngle = self.my_car.angle
        driftAngularSpeed = self.my_car.speedo.angularSpeed()
        driftAngularSpeed2 = self.my_car.speedo.angularAccel()
        radius = pieces[pieceIndex].radius()

        self.driftSamples.append(driftAngle)
        self.driftAccelSamples.append(driftAngularSpeed2)
        self.radiusSamples.append(radius)
        print "         %f\t %f\t %f\t %f\t %f" % (speed, radius, driftAngle, driftAngularSpeed, driftAngularSpeed2)


bot_class = RaceBot
