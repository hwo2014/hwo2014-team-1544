from BotBase import BotBase

class NoobBot(BotBase):

    def __init__(self, socket, name, key):
        BotBase.__init__(self, socket, name, key)

    def on_car_positions(self, data):
        self.throttle(0.5)

bot_class = NoobBot
