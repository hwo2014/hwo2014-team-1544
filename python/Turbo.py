# class DumbTurboAI():
#     def __init__(self, bot):
#         self.bot = bot

#     def tick(self):
#         if not self.bot.stored_turbo or self.bot.active_turbo:
#             return
#         self.bot.activate_turbo()

# TODO: Should be a shared function.  This is duplicated

from TrackObjects import Piece

def error(msg):
    sys.exit(msg)

class TurboAI():

    WEIGHT = 1.0

    def __init__(self, bot):
        self.bot = bot
        self.track_piece_ratings = None

    def have_turbo_option(self):
        return self.bot.stored_turbo and not self.bot.active_turbo

    # Making this up as I go along right now.  Rate each piece of the track from
    # 1-10.  10 meaning definitely turbo and 0 meaning definitely not.
    # Inbetween will depend on some sort of probability and threshold.  I'm
    # thinking this will change wrt how long we've held the turbo for.
    # Ideally this function would think through each pience and figure out the
    # maximum velocity of all upcoming corners.  Do this over some "calibrated"
    # distance and we can have a good stab at whether it's a good place to turbo.
    # TODO: Make this not shit!
    def get_track_piece_ratings(self):
        if self.track_piece_ratings != None:
            return self.track_piece_ratings

        self.track_piece_ratings = []
        num_pieces = len(self.bot.track_data.pieces)
        pieces = self.bot.track_data.pieces

        # For now this rates 0 for a corner, 10 for a straight followed by two
        # more straights and 5 for any other straight.
        for iPiece in range(0, num_pieces):
            if pieces[iPiece].piece_type == Piece.TYPE_CORNER:
                self.track_piece_ratings.append(0)
                continue

            if pieces[iPiece].piece_type == pieces[(iPiece + 1) % num_pieces].piece_type and\
             pieces[iPiece].piece_type == pieces[(iPiece + 2) % num_pieces].piece_type:
                self.track_piece_ratings.append(10)
                continue
            self.track_piece_ratings.append(5)
        return self.track_piece_ratings

    def hold_first_place(self):
        # Look at current position, length of time turbo was held and quality
        # of track overtaking.
        if not self.have_turbo_option():
            return

        piece_rating = self.get_track_piece_ratings()[self.bot.my_car.current_position().piece_index]

        if piece_rating == 10:
            self.bot.activate_turbo()


# TODO: Verify that states work well between qualify and race.
class Turbo():

    def __init__(self, turbo_data, tick_acquired):
        self.tick_acquired = tick_acquired
        # I doubt the ms duration is of any use to us, but store it nonetheless.
        self.duration_milliseconds = turbo_data['turboDurationMilliseconds']
        self.duration_ticks = turbo_data['turboDurationTicks']
        self.turbo_factor = turbo_data['turboFactor']
        self.tick_started = None

    def __str__(self):
        return "Turbo:\nFactor = %s\nDuration = %s" % (self.turbo_factor, self.duration_ticks)

    def start(self, tick):
        self.tick_started = tick

    def exhausted(self, tick):
        if tick - self.tick_started >= self.duration_ticks:
            return True
        return False

    def get_turbo_factor(self):
        return self.turbo_factor
