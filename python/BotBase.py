from abc import abstractmethod
import json
import os

class BotBase(object):

    def __init__(self, socket, name, key):
        self.socket = socket
        self.name = name
        self.key = key
        self.useGUI = False
        self.tkMaster = None
        self.gameTick = 0

    # This is the only function we don't have a default implementation for.
    # It's fundamental to the bot so there's no point trying to have a default.
    @abstractmethod
    def on_car_positions(self, data):
        pass

    # All the on_xxx functions are responders to server messages.  All messages
    # documented in the Tech Specs have a default here except on_car_positions
    # above.  They fire infrequently enough to mean logging shouldn't annoy the
    # servers too much.
    def on_join(self, data):
        print("Doing nothing but ping.")
        self.ping()

    def on_your_car(self, data):
        print("Doing nothing but ping.")
        self.ping()

    def on_game_init(self, data):
        print("Doing nothing but ping.")
        self.ping()

    def on_game_start(self, data):
        print("Doing nothing but ping.")
        self.ping()

    def on_lap_finished(self, data):
        print("Doing nothing but ping.")
        self.ping()

    def on_finish(self, data):
        print("Doing nothing but ping.")
        self.ping()

    def on_crash(self, data):
        print("Doing nothing but ping.")
        self.ping()

    def on_spawn(self, data):
        print("Doing nothing but ping.")
        self.ping()

    def on_game_end(self, data):
        print("Doing nothing but ping.")
        self.ping()

    def on_error(self, data):
        print("Got error: {0}".format(data))
        self.ping()

    def on_tournament_end(self, data):
        print("Doing nothing but ping.")
        self.ping()

    def on_turbo_available(self, data):
        print("Doing nothing but ping.")
        self.ping()

    def on_turbo_start(self, data):
        print("Doing nothing but ping.")
        self.ping()

    def on_turbo_end(self, data):
        print("Doing nothing but ping.")
        self.ping()

    def on_dnf(self, data):
        # It's highly unlikely we'll ever receive this as the changes are our
        # bot code has failed somehow.
        print("Doing nothing but ping.")
        self.ping()

    def run(self):
        testAction = os.environ.get("TEST_ACTION", None)
        trackName = os.environ.get("TRACK_NAME", None)
        password = os.environ.get("PASSWORD", None)
        carCount = int(os.environ.get("CAR_COUNT", 0))
        testBotName = os.environ.get("TEST_BOT_NAME", "Ralph")

        if testAction == "Create":
            self._create_race(testBotName, trackName, password, carCount)
        elif testAction == "Join":
            self._joinRace(testBotName, trackName, password, carCount)
        else:
            self._join()

        self._msg_loop()

    # Messaging helpers
    def send(self, msg):
        self.socket.send(msg + "\n")

    def msg(self, msg_type, data):
        self.send(json.dumps({"msgType": msg_type, "data": data}))

    def ping(self):
        self.msg("ping", {})

    def throttle(self, throttle):
        self.msg("throttle", throttle)

    def turbo(self):
        print("Sending turbo message")
        self.msg("turbo", "Super Go Drift Team Action Turbo Power!")

    def switch_lane(self, toRight):
        direction = "Right" if toRight else "Left"
        print "Request switch Lane: ", direction
        self.msg("switchLane", direction)

    # Private functions
    def _join(self):
        print "_join"
        return self.msg("join", {"name": self.name,
                                 "key": self.key})

    # Join test race
    def _joinRace(self, testBotName, trackName=None, password=None, carCount=0):
        join_msg = {  "botId":  {
                        "name": testBotName,
                        "key": self.key
                      }
                   }

        if trackName:
            join_msg["trackName"] = trackName
        if password:
            join_msg["password"] = password

        join_msg["carCount"] = carCount

        print join_msg

        self.msg("joinRace", join_msg)

    def _create_race(self, testBotName, trackName=None, password=None, carCount=0):
        trackName = os.environ.get("TRACK_NAME", "germany")

        create_msg =  { "botId": {
                            "name": testBotName,
                             "key": self.key
                            }
                      }
        if trackName:
            create_msg["trackName"] = trackName
        if password:
            create_msg["password"] = password

        create_msg["carCount"] = carCount

        print create_msg

        self.msg("createRace", create_msg)

    def _msg_loop(self):
        # msg_map is a map from server message to a pair where the first element
        # is the function to call in response to the message and the second is
        # whether to log the name of the message being received.  We don't want
        # to log every message (at least on_car_positions) else we'll generate
        # spam.
        msg_map = {
            'join': (self.on_join, True),
            'yourCar': (self.on_your_car, True),
            'gameInit': (self.on_game_init, True),
            'gameStart': (self.on_game_start, True),
            'carPositions': (self.on_car_positions, False),
            'lapFinished': (self.on_lap_finished, True),
            'finish': (self.on_finish, True),
            'crash': (self.on_crash, True),
            'spawn': (self.on_spawn, True),
            'gameEnd': (self.on_game_end, True),
            'error': (self.on_error, True),
            'tournamentEnd': (self.on_tournament_end, True),
            'dnf': (self.on_dnf, True),
            'turboAvailable': (self.on_turbo_available, True),
            'turboStart': (self.on_turbo_start, True),
            'turboEnd': (self.on_turbo_end, True),
        }

        def processSocketLine(line):
            msg = json.loads(line)
            msg_type, data = msg['msgType'], msg['data']
            self.gameTick = msg.get('gameTick', self.gameTick)
            if msg_type in msg_map:
                # Optinally log the message name here
                if msg_map[msg_type][1]:
                    print("Got %s" % msg_type)
                # Call the callback, i.e on_xxx
                msg_map[msg_type][0](data)
            else:
                # According to the tech specs, this should never happen now.
                print("Got unhandled message {0}".format(msg_type), data)
                self.ping()

        socket_file = self.socket.makefile()

        if not self.useGUI:
            while True:
                line = socket_file.readline()
                if not line:
                    break
                processSocketLine(line)
        else:
            # For drawing Tkinter needs to use its own loop
            # We need to wrap processSocketLine inside a socketTick callback
            # called at the end of every loop iteration

            # import here since this module isn't available on the CI server
            from Tkinter import Tk
            def socketTick(tkMaster, socket_file):
                line = socket_file.readline()
                if line:
                    processSocketLine(line)
                    tkMaster.after(0, socketTick, tkMaster, socket_file)
                else:
                    # this will stop the Tkinter loop
                    tkMaster.destroy()

            print "Using Tkinter loop"

            self.tkMaster = Tk()
            self.tkMaster.after(0, socketTick, self.tkMaster, socket_file)
            self.tkMaster.mainloop()
