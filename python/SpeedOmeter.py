from TrackObjects import Position

# TODO: Document that speedos return the speed in units / 1/60 of a second.
class SpeedOmeter:

    def __init__(self, track_data, guide_flag_position):
        # Constants
        self.track_data = track_data

        # Variables
        self.dirty = True
        self._speed = 0
        self._accel = 0
        self._prevSpeed = 0
        self._prevAccel = 0
        self._accelDelta = 0

        self._driftAngle = 0
        self._angularSpeed = 0
        self._angularAccel = 0

        self.lastGameTick = 0

        # This is a guess at the initial position.  Means the speedo doesn't
        # need to have a special case for previous_position being None.
        # TODO: Verify the validaty of this
        """
        start_position = {
            "pieceIndex":0,
            "inPieceDistance":0,
            "lane": {
                "startLaneIndex": 0,
                "endLaneIndex": 0
            },
            "lap": 0
        }
        """

        self.current_position = None
        self.previous_position = None
  
    def set_position(self, position, angle, gameTick):
        self.previous_position = self.current_position
        self.current_position = position
        self._prevDriftAngle = self._driftAngle
        self._driftAngle = angle
        self._compute_speed(gameTick)
        #self.dirty = True

    def speed(self):
        #if self.dirty:
        #    self._compute_speed()
        return self._speed

    def accel(self):
        return self._accel

    def prevSpeed(self):
        #if self.dirty:
        #    self._compute_speed()
        return self._prevSpeed

    def prevAccel(self):
        return self._prevAccel

    def accelDelta(self):
        return self._accelDelta

    def angularSpeed(self):
        return self._angularSpeed

    def angularAccel(self):
        return self._angularAccel

    def laneChanged(self):
        if self.current_position and self.previous_position:
            return self.previous_position.start_lane_index != self.current_position.end_lane_index 
        return False

    def _compute_speed(self, gameTick):

        if not self.current_position or not self.previous_position:
            return

        elapsedTicks = gameTick - self.lastGameTick
        self.lastGameTick = gameTick

        if elapsedTicks < 1:
            return

 
        self._prevSpeed = self._speed
        self._prevAccel = self._accel


        distanceTravelled = self.track_data.distanceBetweenPositions(self.previous_position, self.current_position)

        self._speed = distanceTravelled / elapsedTicks

        #if self.previous_position.piece_index == self.current_position.piece_index:
        #    # TODO: Handle switch speeds properly (are they different?)
        #    self._speed = (self.current_position.in_piece_distance - self.previous_position.in_piece_distance) / ticksPassed
        #else:
        #    # TODO: Only using start_lane_index here!
        #    lane_index = self.previous_position.start_lane_index
        #    self._speed = (self.track_data.pieces[self.previous_position.piece_index].length(lane_index) - self.previous_position.in_piece_distance + self.current_position.in_piece_distance) / ticksPassed

        self._accel = (self._speed - self._prevSpeed) / elapsedTicks
        self._accelDelta = (self._accel - self._prevAccel) / elapsedTicks

        # Calculate angular differentials
        old_angularSpeed = self._angularSpeed
        self._angularSpeed = (self._driftAngle - self._prevDriftAngle) / elapsedTicks
        self._angularAccel = (self._angularSpeed - old_angularSpeed) / elapsedTicks

        #self.dirty = False
