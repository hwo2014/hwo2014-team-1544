import os
import socket
import sys
import optparse

def error(msg):
    sys.exit(msg)

def get_bot_class():
    """Looks for the environment variable BOT_CLASS.  This should specify the
       name of the bot class to use for the run.  If not specified this defaults
       to RaceBot.  The function then imports a module with the same name as the
       class and returns the bot_class variable from that module.  This variable
       should be a class object and will be used to instantiate the bot."""

    if not "BOT_CLASS" in os.environ:
        print("No BOT_CLASS specified.  Using RaceBot.")
        bot_class = "RaceBot"
    else:
        bot_class = os.environ["BOT_CLASS"]

    try:
        bot_class_module = __import__(bot_class)
        bot_class = bot_class_module.bot_class
    except Exception, e:
        error("Failed to import BOT_CLASS [%s]\n Exception was:\n %s"
            % (bot_class, e))

    print("Using BOT_CLASS %s." % bot_class)
    return bot_class

if __name__ == "__main__":
    # Custom bot class can be specified using the BOT_CLASS environment
    # variable.  Using env variable so as no to interfere with CI.
    bot_class = get_bot_class()

    if len(sys.argv) != 5:
        print("Usage: ./run host port botname botkey")
    else:
        # aparker: Added an extra command line option: botpythonfile.
        # This allows us to easily test different bots from the command line.
        host, port, name, key = sys.argv[1:5]
        print("Connecting with parameters:")
        print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, int(port)))
        print("Connected successfully.  Creating bot class.")
        
        drawGUI = os.environ.get("DRAW_GUI", "0") == "1"

        print "drawGUI:", drawGUI
        bot = bot_class(s, name, key)
        bot.useGUI = drawGUI
        bot.run()
