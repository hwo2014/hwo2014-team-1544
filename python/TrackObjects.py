import math
from vec2d import vec2d
from collections import OrderedDict

class Lane:
    def __init__(self, data):
        self.distance_from_center = data["distanceFromCenter"]
        self.index = data["index"]

    def __str__(self):
        return "Lane:\nindex = %s\ndistance_from_center = %s" % \
        (self.index, self.distance_from_center)

class Piece:

    TYPE_STRAIGHT = 0
    TYPE_CORNER = 2

    def __init__(self, lanes, piece_type):
        self.lanes = lanes
        self.piece_type = piece_type
        self.hasSwitch = False
        self._startPos = vec2d(0, 0)
        self._startPosOfLanes = {}
        self.startDir = vec2d(0, 0)

    def __str__(self):
        return "Piece:\ntype = %s\nlength = %s\nstartPos = %s " % (
                        self._type_to_string(), self.length(), self.startPos())

    def _type_to_string(self):
        if self.piece_type == Piece.TYPE_STRAIGHT:
            return "TYPE_STRAIGHT"
        elif self.piece_type == Piece.TYPE_CORNER:
            return "TYPE_CORNER"

    def startPos(self, lane_index=-1):
        if lane_index == -1:
            return self._startPos
        else:
            return self._startPosOfLanes[lane_index]

    def percentage_into_piece(self, distance):
        length = self.length()
        if distance < 0 or distance > length:
            print("Error: Tried to compute percentage of distance into a piece but distance in was out of bounds")
            return 0 if distance < 0 else 1.0
        return distance / length

class Straight(Piece):

    def __init__(self, piece, lanes):
        Piece.__init__(self, lanes, Piece.TYPE_STRAIGHT)
        self._length = piece["length"]

    def length(self, lane_index=-1):
        # lane length always equals track length
        return self._length

# TODO: This uses Straight.length to return it's length.  It's possibly
# wrong as the switch must add distance.  Although maybe it
# doesn't.  Can you switch and fly off?  I'm going to bet no.

class Corner(Piece):
    def __init__(self, piece, lanes):
        Piece.__init__(self, lanes, Piece.TYPE_CORNER)
        self._radius = piece["radius"]
        self.angle = piece["angle"]
        self._lane_radii = {}
        self._lane_lengths = {}
        if self.angle >= 0:
            # We have a right turn
            angle_sign = 1.0
        else:
            # We have a left turn
            angle_sign = -1.0

        abs_angle_radians = angle_sign * math.radians(self.angle)

        self._length = self._radius * abs_angle_radians

        for lane in self.lanes:
            # Note that the distance_from_center of a lane is +ve if it's to the
            # right and negative if it's to the left.  That's why sign matters!
            lane_radius = self._radius - angle_sign * lane.distance_from_center

            self._lane_radii[lane.index] = lane_radius
            self._lane_lengths[lane.index] = lane_radius * abs_angle_radians

    def __str__(self):
        return "%s\nradius = %s\nangle = %s" % (Piece.__str__(self), self.radius, self.angle)

    def length(self, lane_index=-1):
        if lane_index == -1:
            return self._length
        else:
            return self._lane_lengths[lane_index]

    def radius(self, lane_index=-1):
        if lane_index == -1:
            return self._radius
        else:
            return self._lane_radii[lane_index]

    # Angle from start point to given position on the bend
    def angleToPosition(self, position):
        laneIndex = position.start_lane_index
        return (position.in_piece_distance / self.length(laneIndex)) * self.angle

    #def lengthToInPieceDistance(self, position):
    #    laneIndex = position.start_lane_index
    #    return (position.in_piece_distance / self.length(laneIndex)) * self.angle

class Position:
    def __init__(self, position_data):
        self.piece_index = position_data["pieceIndex"]
        self.in_piece_distance = position_data["inPieceDistance"]
        self.start_lane_index = position_data["lane"]["startLaneIndex"]
        self.end_lane_index = position_data["lane"]["endLaneIndex"]
        self.lap = position_data["lap"]
    def __str__(self):
        return "Position Object\npiece_index = %s\nin_piece_distance = %s\nstart_lane_index = %s\nend_lane_index = %s\nlap = %s\n" %\
            (self.piece_index, self.in_piece_distance, self.start_lane_index, self.end_lane_index, self.lap)


# Start point of track (for visualisation)
class StartPoint:
    x = 0
    y = 0
    angle = 0
    def __init__(self, startingPointData):
        self.x = startingPointData["position"]["x"]
        self.y = startingPointData["position"]["y"]
        self.angle = startingPointData["angle"]

    def __str__(self):
        return "TrackData: x: %f, y: %f, angle: %f" % (self.x, self.y, self.angle)

class TrackData:

    def __init__(self, track_data):
        print("Creating TrackData")
        # TODO: Verify that the lanes are in index order (will that ever matter
        # to us?).
        self.lanes = []
        for lane in track_data["lanes"]:
            self.lanes.append(Lane(lane))

        print("Number of lanes = %s" % len(self.lanes))

        # Needed for drawing track
        self.startPoint = StartPoint(track_data["startingPoint"])

        curPos  = vec2d(self.startPoint.x, self.startPoint.y)
        curAngle = math.radians(self.startPoint.angle)
        curDir  = vec2d(math.cos(curAngle), math.sin(curAngle))

        self.pieces = []

        for piece in track_data["pieces"]:

            if "radius" in piece:
                trackPiece = Corner(piece, self.lanes)
            else:
                trackPiece = Straight(piece, self.lanes)

            if "switch" in piece:
                trackPiece.hasSwitch = True

            self.pieces.append(trackPiece)

        self.number_of_pieces = len(self.pieces)

        print("Number of pieces = %s" % self.number_of_pieces)

        self.compositeBends = self.getCompositeBends()
        print("Composite bends = %s" % self.compositeBends)

    # Total turning angle between start of 2 pieces or position on the second piece
    # if piece2Position is provided
    def angleBetweenPieces(self, pieceIndex1, pieceIndex2, piece2Position = None):
        if not (pieceIndex1 < len(self.pieces) and pieceIndex2 < len(self.pieces)):
            return 0
        totalAngle = 0
        numPieces = pieceIndex2 - pieceIndex1
        for i in range(0, numPieces):
            index = (pieceIndex1 + i) % len(self.pieces)
            piece = self.pieces[index]
            if piece.piece_type == Piece.TYPE_CORNER:
                totalAngle = totalAngle + piece.angle

        if piece2Position:
            piece = self.pieces[pieceIndex2]
            if piece.piece_type == Piece.TYPE_CORNER:
                totalAngle = totalAngle + piece.angleToPosition(piece2Position)
                #print piece.angleToInPieceDistance(piece2Position)
        return totalAngle


    # # distance between 2 positions (assumes they are in the same lane and position2 is ahead of position1)
    def distanceBetweenPositions(self, position1, position2):
        if position1.start_lane_index != position2.start_lane_index:
            print "distanceBetweenPositions: warning, positions are in different lanes!"

        if position1.piece_index == position2.piece_index:
            return max(position2.in_piece_distance - position1.in_piece_distance, 0)

        totalDistance = 0
        lane = position1.start_lane_index

        numPieces = (position2.piece_index - position1.piece_index + 1) % len(self.pieces)

        for i in range(0, numPieces):
            index = (position1.piece_index + i) % len(self.pieces)
            piece = self.pieces[index]

            # first piece
            if index == position1.piece_index:
                totalDistance += piece.length(lane) - position1.in_piece_distance
            #last piece
            elif index == position2.piece_index:
                totalDistance += position2.in_piece_distance
            #in between piece
            else:
                totalDistance += piece.length(lane)

        return totalDistance

    # Get next N piece indices from given position which have lane switches (including the one for the position)
    def getNextSwitches(self, position, numSwitches=1):
        switchPieceIndices = []
        i = 0
        while numSwitches > 0:
            index = (position.piece_index + i) % len(self.pieces)
            piece = self.pieces[index]
            if piece.hasSwitch:
                switchPieceIndices += [index]
                numSwitches -= 1
            i += 1

        return switchPieceIndices

    # Get next corner piece for a given position (including one the one for the position)
    def getNextCorner(self, position):
        for i in range(0, len(self.pieces)):
            index = (position.piece_index + i) % len(self.pieces)
            piece = self.pieces[index]
            if piece.piece_type == Piece.TYPE_CORNER:
                return index

        # This should never happen
        return 0


    def getTotalLengthOfPieces(self, pieceIndices, laneIndex):
        totalLength = 0
        for i in pieceIndices:
            totalLength += self.pieces[i].length(laneIndex)

        return totalLength

    # Get indices of the next composite bend, including the one for the position
    # A composite bend is made up of consecutive corner pieces of the same turning radius
    def getNextCompositeBend(self, position):
        pieceIndices = []
        # Get first corner piece
        firstIndex = self.getNextCorner(position)
        firstPiece = self.pieces[firstIndex]
        if firstPiece.piece_type != Piece.TYPE_CORNER:
            # This shouldn't happen
            return [] 

        # Get next consecutive corner pieces with same turning radius R 
        R = firstPiece.radius(position.end_lane_index)

        for i in range(0, len(self.pieces)):
            index = (firstIndex + i) % len(self.pieces)
            piece = self.pieces[index]
            if piece.piece_type == Piece.TYPE_CORNER and piece.radius(position.end_lane_index) == R:
                pieceIndices += [index]
            else:
                break

        return pieceIndices 

    def getCompositeBend(self, firstPieceIndex):
        firstPiece = self.pieces[firstPieceIndex]
        if firstPiece.piece_type != Piece.TYPE_CORNER:
            return []
        R = firstPiece.radius()
        Direction = firstPiece.angle >= 0
        print Direction
        pieceIndices = []
        for i in range(0, len(self.pieces)):
            index = (firstPieceIndex + i) % len(self.pieces)
            piece = self.pieces[index]            
            if piece.piece_type == Piece.TYPE_CORNER and piece.radius() == R and ((piece.angle >= 0) == Direction):
                pieceIndices += [index]
            else:
                break
        return pieceIndices

    def getCompositeBends(self):
        bends = []
        i = 0
        while i < len(self.pieces):
            piece = self.pieces[i]
            if piece.piece_type == Piece.TYPE_CORNER:
                bend = self.getCompositeBend(i)
                if len(bend) > 0:
                    bends += [bend]
                    i = bend[-1]
            i += 1

        return bends

    # Get length of next composite bend, or remaining length if in the middle of one
    def getLengthOfNextCompositeBend(self, position):
        pieceIndices = self.getNextCompositeBend(position)   
        if not len(pieceIndices) > 0:
            return 0

        totalLength = 0
        firstIndex = pieceIndices[0]
        firstPiece = self.pieces[firstIndex]

        # if we are already on the first piece take into account inpiece distance 
        if firstIndex == position.piece_index:
            totalLength = firstPiece.length(position.end_lane_index) - position.in_piece_distance

        # add lengths of remaning pieces
        totalLength += self.getTotalLengthOfPieces(pieceIndices[1:], position.end_lane_index)

        return totalLength

    def getDistanceFromNextBend(self, position):
        pieceIndices = []
        # Get first corner piece
        firstIndex = position.piece_index
        firstPiece = self.pieces[firstIndex]
        if firstPiece.piece_type != Piece.TYPE_STRAIGHT:
            # This shouldn't happen
            return 0 

        # Get next consecutive straight pieces 
        for i in range(0, len(self.pieces)):
            index = (firstIndex + i) % len(self.pieces)
            piece = self.pieces[index]
            if piece.piece_type == Piece.TYPE_STRAIGHT:
                pieceIndices += [index]
            else:
                break

        # Get total distance
        totalLength = 0
        # if we are already on the first piece take into account inpiece distance 
        if firstIndex == position.piece_index:
            totalLength = firstPiece.length(position.end_lane_index) - position.in_piece_distance

        # add lengths of remaning pieces
        totalLength += self.getTotalLengthOfPieces(pieceIndices[1:], position.end_lane_index)

        return totalLength

        


    # Get list of lanes ordered by shortest path first betweem the start of pieceIndex1 pieceIndex2
    def getLanesLengthsBetweenPieces(self, pieceIndex1, pieceIndex2):
        laneLengths = {}
        for l in range(0, len(self.lanes)):
            laneLengths[l] = 0

        index = pieceIndex1
        while index != pieceIndex2:
            piece = self.pieces[index]
            for laneIndex in range(0, len(self.lanes)):
                laneLengths[laneIndex] += piece.length(laneIndex)

            index = (index + 1) % len(self.pieces)

        # Sort by lane lengths
        return OrderedDict(sorted(laneLengths.items(), key=lambda x: x[1]))

    # Get remaining distance of consecutive straights including current one
    def getDistanceToEndOfStraight(self, position):
        totalLength = 0
        currentPiece = self.pieces[position.piece_index]
        totalLength += currentPiece.length() - position.in_piece_distance

        for i in range(1, len(self.pieces)):
            index = (position.piece_index + i) % len(self.pieces)
            piece = self.pieces[index]
            if piece.piece_type == Piece.TYPE_STRAIGHT:
                totalLength += piece.length()
            else:
                break
        return totalLength

    def __str__(self):
        retval = "TrackData:\n"
        retval = "Lanes:\n"
        for lane in self.lanes:
            retval += "%s\n" % lane
        retval = "Pieces:\n"
        for piece in self.pieces:
            retval += "%s\n" % lane
        return retval
