class Car:
    def __init__(self, data):
        # These are constants
        self.name = data["id"]["name"]
        self.color = data["id"]["color"]
        self.length = data["dimensions"]["length"]
        self.width = data["dimensions"]["width"]
        self.guide_flag_position = data["dimensions"]["guideFlagPosition"]

        # These are variables
        #self.track_piece = 0
        self.angle = 0
        self.speedo = None
        self.position = None
        self.percentage_into_piece_dirty = True
        self.percentage_into_piece = 0
        print("Created Car: name = %s, color = %s" % (self.name, self.color))

    def __str__(self):
        return "%s: %s" % (self.name, self.color)

    def set_position(self, piece_position_data, angle, gameTick):
        self.position = piece_position_data
        self.angle = angle
        self.speedo.set_position(self.position, angle, gameTick)
        self.percentage_into_piece_dirty = True

    def create_speedo(self, speedo_class, track_data):
        self.speedo = speedo_class(track_data, self.guide_flag_position)

    def current_position(self):
        return self.position

    def percentage_into_track_piece(self, track_data):
        if not self.percentage_into_piece_dirty:
            return self.percentage_into_piece

        self.percentage_into_piece = track_data.pieces[self.position.piece_index].percentage_into_piece(self.position.in_piece_distance)
        self.percentage_into_piece_dirty = False
        return self.percentage_into_piece

    def current_angle(self):
        return self.angle
