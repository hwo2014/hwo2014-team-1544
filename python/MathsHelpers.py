from TrackObjects import Position, TrackData, Piece

import math

# Check that list of numbers are within a certain error value of each other
def isUniform(numList, error):
    return len(numList) > 0 and all(abs(x - numList[0]) < error for x in numList)    

# Vector maths - not built into python unless we abuse complex numbers

def unitVector(x, y):
    l = (x**2 + y**2)**0.5
    return (x/l, y/l)

def perpVector(x, y, sign):
    return (y*-sign, x*sign)


def calculateDriveAndFrictionCoefficents(t, v1, a1, v2, a2):
    # A1 = T*DK + V1*FK
    # A2 = T*DK + V2*FK
    # T*DK = A1 - V1*FK  
    #      = A2 - V2*FK
    # V2*FK - V1*FK = A2 - A1 
    # (V2 - V1)*FK = A2 - A1
    # FK = (A2 - A1) / (V2 - V1)
    # DK = (A2 - V2*FK) / T

    print "calculateDriveAndFrictionCoefficents:", t, v1, a1, v2, a2

    frictionCoefficient = (a2 - a1) / (v2 - v1)
    driveCoefficient = (a2 - v2*frictionCoefficient) / t

    return driveCoefficient, frictionCoefficient



# Calculate throttle needed to reach target cruise speed from current speed in the quickest time
# Setting allowOverShoot to False will attempt to prevent the overshooting targetSpeed but convergence will
# be a little slower
def calculateCruiseThrottle(currentSpeed, targetSpeed, frictionCoefficient, driveCoefficient, allowOverShoot=True):
    # A: accel,  V: velocity, F: friction coefficient D: drive coefficient, T: throttle)
    # Assumes frictionCoefficient is negative

    # This is the max acceleration we want to achieve (i.e to reach target speed in 1 timestep)
    targetAccel = targetSpeed - currentSpeed

    if not allowOverShoot and abs(targetAccel) < (targetSpeed * 0.02):
        # We are nearing our target velocity use the cruise throttle value instead
        # Throttle to maintain cruise speed
        # T = -V*F / D
        throttle = -(targetSpeed * frictionCoefficient) / driveCoefficient
    else:
        # Throttle to reach cruise speed as quickly as possible
        # T = A - V*F / D
        throttle = (targetAccel - currentSpeed * frictionCoefficient) / driveCoefficient
        #print "allow over shoot", throttle

    return throttle

def calculateMaxSpeedForDrift(position, track_data, speed, accel, driftAngle, driftAngularSpeed, driftAngularSpeed2, frictionCoefficient, Co2, Co3, shift):
    pieceIndex = position.piece_index

    nextCornerStartIndex = track_data.getNextCompositeBend(position)[0]
    nextPiece = track_data.pieces[nextCornerStartIndex]

    nextR  = nextPiece.radius(position.start_lane_index)

    maxDriftAngle = driftAngle

    if nextPiece.angle >= 0:
        nextAngleSign = 1
    else:
        nextAngleSign = -1

    thisV = speed

    thisA = accel
    nextV = thisV
    nextA = frictionCoefficient * nextV
    thisDri = driftAngle
    thisW = driftAngularSpeed
    thisW2 = driftAngularSpeed2
    nextDri = thisDri + thisW
    nextW2 = thisW2
    nextW = nextW2 + thisW

    maxDriftAngle = nextDri

    distance = 0

    if thisV > 5:
        for i in range(1, 50):

            thisV = nextV
            thisA = nextA
            nextV = thisV*thisV/(thisV-thisA)
            nextA = frictionCoefficient * nextV
            thisDri = nextDri
            thisW = nextW
            thisW2 = nextW2
            nextDri = thisDri + thisW
            nextW2 = ((nextV*nextV/nextR * Co2) * math.cos(((abs(nextDri) * 90/shift) + 90) * 0.017453293) + (nextV*nextV/nextR * Co2) + Co3) * (nextAngleSign)
            nextW = nextW2 + thisW

            if abs(nextDri) > maxDriftAngle:
                maxDriftAngle = abs(nextDri)

            distance = distance + nextV
            remainDistance = track_data.getLengthOfNextCompositeBend(position)

            if distance > (remainDistance):
                break


            #print "max drift angle in next corner: %d\t %f\t %f\t %d\t %f\t %f\t %f" % (i, maxDriftAngle, myCar.speedo.speed(), nextCornerStartIndex, track_data.pieces[nextCornerStartIndex].radius(), nextCornerL, remainDistance)

    #print "maxDriftAngle", maxDriftAngle

    maxDriftAngle = abs(maxDriftAngle)

    CRASH_ANGLE = 60 
    if maxDriftAngle > CRASH_ANGLE - 10:
        testSpeed = 0
    elif maxDriftAngle > CRASH_ANGLE - 20:
        testSpeed = speed
    elif maxDriftAngle < CRASH_ANGLE - 30:
        testSpeed = speed + 1
    else:
        if track_data.pieces[pieceIndex].piece_type == Piece.TYPE_CORNER:
            testSpeed = 0
            #print "Break!!!!!!  inital angle : %f\t %f" % (myCar.angle, maxDriftAngle)
        else:
            testSpeed = speed

    return testSpeed


def calculateStraightBoostDistance(targetDistance, frictionCoefficient, driveCoefficient):
    D = targetDistance
    Dk = driveCoefficient
    Fk = frictionCoefficient

    # Calculate distance that need to acceleration at max engine power
    S1 = D / (1 + Dk / Fk)

    return S1




# Work out throttle required to reach targetVelocity from currentVelocity in targetDistance
# The logic is basically to use max throttle until a peak speed Vp and then switch the engine of to decelerate
# as quicky as possible - this should cover the targetDistance in the shortest possible time 
def calculateRampUpAndDownThrotte(currentSpeed,  targetSpeed, targetDistance, frictionCoefficient, driveCoefficient):

    Vo = currentSpeed
    Vf = targetSpeed
    D = targetDistance
    Dk = driveCoefficient
    Fk = frictionCoefficient

    # Calculate peak velocity Vp
    Vp = (D + Dk*Vo + Fk/2 * (Vf**2 - Vo**2)) / Dk

    #print "Vp  is ", Vp

    # Calculate time to reach peak velocity Vp from Vo
    #Tp = ((Vo-Vp) / Fk) * math.log( (Dk - Fk*Vp) / (Dk - Fk * Vo) )

    if Vp - Vo > 0.01:
        # We still have yet to reach Vp so keep max throttle
        return 1.0
    else:
        # We must have reached Vp Switch engine off to decellerate as quickly as possible
        return 0


def calculateDriftCoefficients(speed, driftSamples, driftAccelSamples, radiusSamples):

    print "calculateDriftCoefficients: speed, driftSamples, driftAccelSamples, radiusSamples", speed, "\n", driftSamples, "\n",driftAccelSamples, "\n",radiusSamples

    idx = driftAccelSamples.index(min(driftAccelSamples))
    val = driftAccelSamples[idx]
     
    idxmax = driftAccelSamples.index(max(driftAccelSamples))
    valmax = driftAccelSamples[idxmax]
    driVal = driftSamples[idx]
    print " aa        %f\t %f\t %f\t %f\t %f" % (val, idx, valmax, idxmax, driVal)

    Co2List = []
    Co3List = []
    shiftList = []

    Co1 = 0
    Co2 = 0
    Co3 = 0
    shift = 0

    for i in range(0, len(driftSamples)-1):
        Co1 = speed * speed / radiusSamples[i]
        val0 = ((Co1)* math.cos(((abs(0) * 90/abs(driVal))+90) * 0.017453293))
        valmin = ((Co1)* math.cos(((abs(driVal) * 90/abs(driVal))+90) * 0.017453293))
        #val0 = ((1.32935*self.Co1)* math.cos(((abs(0) * 90/abs(driVal))+90) * 0.017453293)+1.32935*self.Co1 - 0.15)
        ranW2 = valmax - val
        Co2 = ranW2 / (val0 - valmin)
        valmin = ((Co1*Co2) * math.cos(((abs(driVal) * 90/abs(driVal))+90) * 0.017453293)+ Co1*Co2)
        Co3 = val - valmin
        shift = driVal
        Co2List.append(Co2)
        Co3List.append(Co3)
        shiftList.append(shift)
        #print "  iii       %d\t %f\t %f\t %f\t %f" % (i, self.Co1, self.Co2, self.Co3, self.shift)

    Co2 = sum(Co2List) / float(len(Co2List))
    Co3 = sum(Co3List) / float(len(Co3List))
    shift = sum(shiftList) / float(len(shiftList))

    print "  iii        %f\t %f\t %f\t %f\t %f\t %f\t %f" % (Co1, Co2, Co3, shift, driVal, ranW2, (val0 - valmin))

    return (Co1, Co2, Co3, shift)

