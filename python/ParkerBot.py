import json
import socket
import sys
from BotBase import BotBase
from Car import Car
from SpeedOmeter import SpeedOmeter
from TrackObjects import Position, TrackData

class ParkerBot(BotBase):

    def __init__(self, socket, name, key):
        BotBase.__init__(self, socket, name, key)
        self.speed = 0
        self.track_data = None
        self.our_car_color = ""
        self.cars = {}
        self.tasks = []

    def on_car_positions(self, data):
        for car_data in data:
            car = self.cars[car_data["id"]["color"]]
            car.set_position(Position(car_data["piecePosition"]), car_data["angle"], self.gameTick)
            if self.gameTick % 60 == 0:
                print("%s car speed is %s" % (car_data["id"]["color"], car.speedo.speed()))
            # TODO Use lap data?
        self.throttle(0.6)

    def on_your_car(self, data):
        self.our_car_color = data["color"]
        print("Our car has color [%s]" % self.our_car_color)

    def on_game_init(self, data):
        self.track_data = TrackData(data["race"]["track"])
        for car in data["race"]["cars"]:
            new_car = Car(car)
            new_car.create_speedo(SpeedOmeter, self.track_data)
            self.cars[car["id"]["color"]] = new_car

    def run(self):
        print("Custom run function for ParkerBot")
        BotBase.run(self)

bot_class = ParkerBot
