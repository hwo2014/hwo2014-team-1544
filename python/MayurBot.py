# Bot clas to test on new tracks to validate our assumptions about track physics
# Not optimised to work things out with the least game ticks

from BotBase import BotBase
from Car import Car
from SpeedOmeter import SpeedOmeter
from TrackObjects import Position, TrackData, Piece
from MathsHelpers import isUniform, calculateCruiseThrottle
import math

class MayurBot(BotBase):

    # States for calibrating
    STATE_MAX_THROTTLE = 0
    STATE_CALIBRATE_FRICTION = 1
    STATE_CALIBRATE_ENGINE = 2
    STATE_CALIBRATE_BENDS = 3
    STATE_CRUISE = 4

    STATE_TEST = 5

    FRICTION_TEST_SPEED_THRESHOLD = 0
    MIN_FRICTION_SAMPLES = 2
    MIN_ENGINE_SAMPLES = 1
    MIN_BEND_SAMPLES = 10
    
    TEST_SPEEDS = [6.7, 9, 6.5, 6.5]

    def __init__(self, socket, name, key):
        BotBase.__init__(self, socket, name, key)
        self.speed = 0
        self.track_data = None
        self.our_car_color = ""
        self.cars = {}
        self.tasks = []
        self.trackGUI = None

        self.testSpeedIndex = 0

        # state
        self.state = self.STATE_MAX_THROTTLE

        # Calculate physics
        self.frictionCoefficient = 0
        self.driveCoefficient = 0
        self.calibrateEngineThrottle = 0.1
        self.frictionSamples = []
        self.driveForceSamples = []
        self.driveCoefficientSamples = []

        self.bendSamples = []
        self.crashSamples = []

        self.cruiseThrottle = 0

        self.last_drift = 0
        self.driftAngularSpeed = 0
        self.driftAngularSpeed2 = 0

        self.bendSampleIndex = 0

    def setState(self, state):
        print "State is now %d" % state
        if state == self.STATE_CALIBRATE_FRICTION:
            self.frictionSamples = []
        elif state == self.STATE_CALIBRATE_ENGINE:
            self.engineSamples = []

        self.state = state

    def on_car_positions(self, data):
        for car_data in data:
            car = self.cars[car_data["id"]["color"]]
            car.set_position(Position(car_data["piecePosition"]), car_data["angle"], self.gameTick)
            if self.gameTick % 10 == 0:
                ""
                #print("%s car speed, accel is %s \t %s" % (car_data["id"]["color"], 
                #    car.speedo.speed(), car.speedo.accel()))
                # T1ODO Use lap data?

        speed = self.myCar().speedo.speed()
        accel = self.myCar().speedo.accel()

        # State transitions
        if self.state == self.STATE_MAX_THROTTLE:
            # TODO - Initial speed is 10 : should fix this in speedo
            if speed > self.FRICTION_TEST_SPEED_THRESHOLD and speed != 10:
                self.setState(self.STATE_CALIBRATE_FRICTION)

        elif self.state == self.STATE_CALIBRATE_FRICTION:
            if speed > 0 and accel < 0:
                #print "Friction:  %f :" % (car.speedo.accel() / car.speedo.speed())
                self.frictionSamples.append(car.speedo.accel() / car.speedo.speed())
                if len(self.frictionSamples) > self.MIN_FRICTION_SAMPLES:
                    samplesToSkip = 1
                    cleanSamples = self.frictionSamples[samplesToSkip:]
                    if isUniform(cleanSamples, 0.001):
                        self.frictionCoefficient = cleanSamples[0]
                        print "Found friction coeifficient: %f" % self.frictionCoefficient 
                        self.setState(self.STATE_CALIBRATE_ENGINE)                     
                    else:
                        # This shouln't happen
                        print "Oh no friction isn't linear!" 
                        print cleanSamples                

        elif self.state == self.STATE_CALIBRATE_ENGINE:
            #print "Engine force: %f - %f * %f" % (accel, speed, self.frictionCoefficient)
            #print "Engine force: %f" % (accel - (speed * self.frictionCoefficient))
            if speed > 0 and accel > 0:
                self.driveForceSamples.append(accel - (speed * self.frictionCoefficient))
                if len(self.driveForceSamples) > self.MIN_ENGINE_SAMPLES:
                    samplesToSkip = 1
                    cleanSamples = self.driveForceSamples[samplesToSkip:]
                    if isUniform(cleanSamples, 0.001):
                        print "Throttle, driveForce:  %f, %f" % (self.calibrateEngineThrottle, cleanSamples[0])
                        self.driveCoefficientSamples.append(cleanSamples[0] / self.calibrateEngineThrottle)
                        self.driveForceSamples = []
                        if self.calibrateEngineThrottle >= 0.3:
                            if isUniform(self.driveCoefficientSamples, 0.001):
                                self.driveCoefficient = self.driveCoefficientSamples[0]
                                print "Found driveCoefficient %f" % (self.driveCoefficient)
                                self.setState(self.STATE_CRUISE)
                            else:
                                print "Oh no, drive force isn't linear with respect to throttle!" 
                        else:

                            self.calibrateEngineThrottle = self.calibrateEngineThrottle + 0.2                 
                    else:
                        # This shouln't happen
                        print "Oh no, drive force isn't constant for constant throttle!" 
                        print cleanSamples   

        elif self.state == self.STATE_CRUISE:
            #if testSpeedIndex < 
            testSpeed = self.TEST_SPEEDS[self.testSpeedIndex]

            self.cruiseThrottle = calculateCruiseThrottle(speed, testSpeed, 
                                            self.frictionCoefficient, self.driveCoefficient, allowOverShoot = False);

            self.cruiseThrottle = min(self.cruiseThrottle, 1.0)
            self.cruiseThrottle = max(self.cruiseThrottle, 0.0)

            #print  "targetAccel, self.cruiseThrottle", targetAccel, self.cruiseThrottle
 
        elif self.state == self.STATE_TEST:
            calculated_accel = 0.7 * self.driveCoefficient + speed * self.frictionCoefficient
            real_accel = accel
            #print "real_accel, calculated_accel", real_accel, calculated_accel 


        old_driftAngularSpeed = self.driftAngularSpeed
        self.driftAngularSpeed = self.myCar().angle - self.last_drift
        self.last_drift = self.myCar().angle
        self.driftAngularSpeed2 = self.driftAngularSpeed - old_driftAngularSpeed
        
        #elif self.state == self.STATE_CALIBRATE_BENDS:
        # Test code for gathering drift stats
        positon = self.myCar().position
        pieceIndex = positon.piece_index
        currentPiece = self.track_data.pieces[pieceIndex]

        if True: #speed > 0 and abs(accel) < 0.001:
            if pieceIndex in [4, 5, 6, 7]:#,  14, 15, 16, 17, 19, 20, 21, 22]:
                if currentPiece.piece_type == Piece.TYPE_CORNER: 
                    radius = currentPiece.radius(positon.start_lane_index)
                else:
                    radius = 100000000

                #print "Speed, index, radius, drift angle: %f \t %d \t %f \t %f" % (speed, pieceIndex, radius, self.myCar().angle) 
                
                w = math.radians(self.myCar().angle)
                w1 = math.radians(self.driftAngularSpeed)
                w2 =  math.radians(self.driftAngularSpeed2)

                if abs(w1) < 0.001:
                    print pieceIndex, self.track_data.angleBetweenPieces(4, pieceIndex, positon), radius, speed, accel, w, w1, w2 

                self.bendSampleIndex = self.bendSampleIndex + 1
            else:
                self.bendSampleIndex = 0

        if pieceIndex == 8:
            self.testSpeedIndex = positon.lap + 1

        if self.state == self.STATE_MAX_THROTTLE:
            self.throttle(0.1)
        elif self.state == self.STATE_CALIBRATE_FRICTION:
            self.throttle(0.0)
        elif self.state == self.STATE_CALIBRATE_ENGINE:
            self.throttle(self.calibrateEngineThrottle)
        elif self.state == self.STATE_CRUISE:
            self.throttle(self.cruiseThrottle)
        elif self.state == self.STATE_TEST:
            self.throttle(0.7)

        if self.useGUI:
            from TrackGUI import TrackGUI
            self.trackGUI.update(self.cars)

    def on_crash(self, data):
        if data["color"] == self.our_car_color:
            speedo = self.myCar().speedo
            positon = self.myCar().position
            pieceIndex = positon.piece_index
            currentPiece = self.track_data.pieces[pieceIndex]
            if currentPiece.piece_type == Piece.TYPE_CORNER:
                radius = currentPiece.radius(positon.start_lane_index)
            else:
                radius = 0
            #print "Speed, radius, drift angle: %f \t %f \t %f" % (speed, radius, self.myCar().angle) 

            print "Crashed: speed, radius, drift angle: %f\t%f\t%f\t%f\t%f" % (speedo.speed(), radius, self.myCar().angle, self.driftAngularSpeed, self.driftAngularSpeed2) 

    def on_your_car(self, data):
        self.our_car_color = data["color"]
        print("Our car has color [%s]" % self.our_car_color)

    def on_game_init(self, data):
        self.track_data = TrackData(data["race"]["track"])
        for car in data["race"]["cars"]:
            new_car = Car(car)
            new_car.create_speedo(SpeedOmeter, self.track_data)
            self.cars[car["id"]["color"]] = new_car

        # Update GUI
        if self.useGUI:
            from TrackGUI import TrackGUI
            self.trackGUI = TrackGUI(self.tkMaster)
            self.trackGUI.setTrackData(self.track_data)

    def myCar(self):
        return self.cars[self.our_car_color]

    def run(self):
        print("Custom run function for MayurBot")
        BotBase.run(self)

bot_class = MayurBot
