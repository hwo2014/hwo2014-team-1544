from Turbo import TurboAI

class OptimalLineAI():
    def __init__(self, bot):
        self.bot = bot
        self.turbo_ai = TurboAI(self.bot)

    def tick(self):
        self.bot.decideMove()

# If you call it AI then it's got to be AI! :)
class AI():
    def __init__(self, bot):
        self.bot = bot
        self.turbo_ai = TurboAI(self.bot)
        self.fallback_ai = OptimalLineAI(self.bot)

    def tick(self):
        position = self.bot.race_position()
        if self.bot.race_position() == 1:
            return self.hold_first_place()
        elif position == 2:
            return self.attack_first_place()

        lanes_of_cars_ahead = self.bot.lanes_of_cars_ahead()
        if len(lanes_of_cars_ahead) < 2:
            print("Unexpected number of cars [%s] in TurboAI.tick" % len(lanes_of_cars_ahead))

        #print("No idea what to do next.  Using default AI.")
        self.fallback_ai.tick()

        # I want to handle the case where there's a two car gap in our lane.
        # Or at least get us into that position somehow.
        # if lanes_of_cars_ahead[0] == lanes_of_cars_ahead[1]:
        #     return self.

    def hold_first_place(self):
        # Choose the optimal line.  We should really defer this decision until
        # the last minute, but I don't have code for that.  It might well be
        # that conditions change before we actually reach the switch.  There's
        # no way to cancel a switch (although on a two lane track you
        # technically could I guess). We'll fix the decision for now.  TODO!
        self.turbo_ai.hold_first_place()
        self.bot.decideMove()

    def attack_first_place(self):
        #print("attack_first_place not implemented.  Using default AI.")
        self.fallback_ai.tick()
