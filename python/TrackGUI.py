#!/usr/bin/env python

import Tkinter
from Tkinter import Canvas, Tk
from TrackObjects import TrackData, Piece
from MathsHelpers import unitVector, perpVector
import math
import json

 
class TrackGUI(object):

    debugDraw = True

    canvasWidth = 800
    canvasHeight = 800
 
    # Transform from track Data coords to screen
    offsetX = canvasWidth/2  + 225
    offsetY = canvasHeight/2 + 200
    scale  = 0.65

    # init with TrackData object
    def __init__(self, tkMaster):
        self.tkMaster = tkMaster
        self.canvas = Canvas(tkMaster, width=self.canvasWidth, height=self.canvasHeight)
        self.canvas.configure(background="black")
        self.canvas.pack()
        self.trackData = None
        self.i = 0
        self.car = None
    
    def setTrackData(self, trackData):
        self.trackData = trackData
        startPoint = trackData.startPoint
        lanes = trackData.lanes
        
        curX, curY  = startPoint.x, startPoint.y
        curAngle = math.radians(startPoint.angle)

        # For debugging draw green dot at centre of arc
        if True:#self.debugDraw:
            self.drawDot(curX, curY + 20, "green")

        self.car = self.drawDot(curX, curY, "orange")


        self.drawRect(curX - 10, curY + 25, curX + 10, curY - 25, 0)

        # first draw single line for track centre
        for pieceIndex, piece in enumerate(trackData.pieces):
            #print "Drawing piece: %s" % piece

            if self.debugDraw:
                self.drawDot(curX, curY)
                self.drawText(curX, curY + 20, text=str(pieceIndex))
            

            #print "curAngle: ", math.degrees(curAngle)
            #print "curX, cury" , curX, curY

            if piece.piece_type == Piece.TYPE_STRAIGHT:
                # piece length is hypotenuse, y is opposite, x adjacent
                h = piece.length(0)
                # TODO swapped x and y to work aruond some bug - fix 
                y = h * math.cos(curAngle)
                x = h * math.sin(curAngle)
                
                endX, endY = curX + x, curY + y
                

                # For debugging draw line at centre of the track
                if self.debugDraw:
                    self.drawLine(curX, curY, endX, endY)        
                
                # Draw lanes
                for lane in lanes:
                    offset = lane.distance_from_center
                    vx, vy = perpVector(x, y, 1)
                    vx, vy = unitVector(vx, vy)
                    vx, vy = vx*offset, vy*offset 
                    self.drawLine(curX + vx, curY + vy, endX + vx, endY + vy, colour="orange")        

                curX, curY = curX + x, curY + y
            elif piece.piece_type == Piece.TYPE_CORNER:
                # Angle of curPos to centre of arc
                theta = 2*math.pi - curAngle
                r = piece.radius()
                x = r * math.cos(theta)
                y = r * math.sin(theta)

                arcAngle = math.radians(piece.angle)

                # TODO - make maths a bit nicer
                if (arcAngle < 0):
                    x = x * -1
                    y = y * -1

                arcCentreX, arcCentreY = curX + x, curY + y

                # For debugging draw green dot at centre of arc
                if self.debugDraw:
                    self.drawDot(arcCentreX, arcCentreY, "red")

                # work out angle to draw arc
                # TODO - make maths a bit nicer
                arcStart = curAngle - math.pi/2
                if arcAngle < 0:
                    arcStart = arcStart - math.pi
 
                 # For debugging draw line at centre of the track
                if self.debugDraw:
                    self.drawArc(arcCentreX, arcCentreY, r, arcStart, arcAngle)

                # Draw lanes
                for lane in lanes:
                    offset = lane.distance_from_center
                    #positive lane offsert means to the right of the centre (shorter radius)
                    self.drawArc(arcCentreX, arcCentreY, r-offset, arcStart, arcAngle, colour="orange")

                gamma = math.pi - curAngle - arcAngle
                x = r * math.cos(gamma)
                y = r * math.sin(gamma)

                # TODO - make maths a bit nicer
                if (arcAngle < 0):
                    x = x * -1
                    y = y * -1

                curX, curY = arcCentreX + x, arcCentreY + y
                curAngle = curAngle + arcAngle


    def drawLine(self, x1, y1, x2, y2, colour="red"):
        # Normlise coords
        x1, y1 = self.toScreenSpace(x1, y1)
        x2, y2 = self.toScreenSpace(x2, y2)
        #print x1, y1, x2, y2

        self.canvas.create_line(x1, y1, x2, y2, fill=colour, width=1)        

    # draw arc of circle with centre cx, cy, radius r from angle theta1 to theta2 
    def drawArc(self, cx, cy, r, start, extent, colour="red"):
        # Normlise coords
        cx, cy = self.toScreenSpace(cx, cy)
        r = r * self.scale
        self.canvas.create_arc(cx - r, cy + r, cx + r, cy - r, start=90-math.degrees(start), 
            extent=-math.degrees(extent), style=Tkinter.ARC, outline=colour, width=1)

        #self.canvas.create_rectangle(50, 50, 200, 200, fill="blue")
        #self.canvas.create_arc(50, 50, 200, 200, start=90-110, 
        #    extent=-80, fill="orange")


    def drawDot(self, x, y, colour = "blue"):
        x, y = self.toScreenSpace(x, y)
        radius = 5
        self.canvas.create_oval(x-radius, y-radius, x+radius, y+radius, fill=colour)

    # transform track coord to pixel coords
    def toScreenSpace(self, x, y):
        x1 = x * self.scale + self.offsetX
        y1 = y * self.scale + self.offsetY
        y1 = self.canvasHeight - y1 #
        return (x1, y1)

    def update(self, cars):
        #car = self.canvas.create_rectangle(50, 25, 150, 75, fill="blue")
        #self.canvas.move(car, self.i , 50)
        self.canvas.update()
        #self.tkMaster.after(0, self.update)


    def drawRect(self, x1, y1, x2, y2, angle, colour = "blue"):

        #def rotCoord(x, y, a):
        #    xr = x * math.cos(a) - y * math.sin(a)  
        #    yr = y * math.cos(a) + x * math.sin(a)
        #    return (xr, yr)
        #x1, y1 = rotCoord(x1, y1, angle)  
        #x2, y2 = rotCoord(x2, y2, angle)
        
        x1, y1 = self.toScreenSpace(x1, y1)
        x2, y2 = self.toScreenSpace(x2, y2)

        self.canvas.create_rectangle(x1, y1, x2, y2, fill="blue")

    def drawText(self, x, y, text, colour = "white"):
        x, y = self.toScreenSpace(x, y)
        self.canvas.create_text(x, y, text=text, fill=colour)


if __name__ == "__main__":

    # Use test data to draw
    json_data=open("TestTrack.json")
    trackJson = json.load(json_data)

    master = Tk()
    trackData = TrackData(trackJson["race"]["track"]) 
    pieces = trackData.pieces
    start =  trackData.startPoint

    trackGUI = TrackGUI(master)
    trackGUI.setTrackData(trackData)
    #master.after(0, trackGUI.update)
    master.mainloop()
